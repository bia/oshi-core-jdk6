package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Oshi Core (JDK6) library.
 * 
 * @author Stephane Dallongeville
 */
public class OshiCoreJDK6Plugin extends Plugin implements PluginLibrary
{
    //
}
